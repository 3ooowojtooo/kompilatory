package com.quary.wojciech.custom;

import com.quary.wojciech.generated.NoKeyboardBaseListener;
import com.quary.wojciech.generated.NoKeyboardParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashMap;
import java.util.Map;

public class MyNoKeyboardListener extends NoKeyboardBaseListener {

    @Override
    public void enterProgram(NoKeyboardParser.ProgramContext ctx) {
        super.enterProgram(ctx);
    }

    @Override
    public void exitProgram(NoKeyboardParser.ProgramContext ctx) {
        super.exitProgram(ctx);
    }

    @Override
    public void enterStatement(NoKeyboardParser.StatementContext ctx) {
        super.enterStatement(ctx);
    }

    @Override
    public void exitStatement(NoKeyboardParser.StatementContext ctx) {
        super.exitStatement(ctx);
    }

    @Override
    public void enterAssign(NoKeyboardParser.AssignContext ctx) {
        super.enterAssign(ctx);
    }

    @Override
    public void exitAssign(NoKeyboardParser.AssignContext ctx) {
        super.exitAssign(ctx);
    }

    @Override
    public void enterPrint(NoKeyboardParser.PrintContext ctx) {
        super.enterPrint(ctx);
    }

    @Override
    public void exitPrint(NoKeyboardParser.PrintContext ctx) {
        super.exitPrint(ctx);
    }

    @Override
    public void enterLoop(NoKeyboardParser.LoopContext ctx) {
        super.enterLoop(ctx);
    }

    @Override
    public void exitLoop(NoKeyboardParser.LoopContext ctx) {
        super.exitLoop(ctx);
    }

    @Override
    public void enterExp(NoKeyboardParser.ExpContext ctx) {
        super.enterExp(ctx);
    }

    @Override
    public void exitExp(NoKeyboardParser.ExpContext ctx) {
        super.exitExp(ctx);
    }

    @Override
    public void enterIf_block(NoKeyboardParser.If_blockContext ctx) {
        super.enterIf_block(ctx);
    }

    @Override
    public void exitIf_block(NoKeyboardParser.If_blockContext ctx) {
        super.exitIf_block(ctx);
    }

    @Override
    public void enterFunction(NoKeyboardParser.FunctionContext ctx) {
        super.enterFunction(ctx);
    }

    @Override
    public void exitFunction(NoKeyboardParser.FunctionContext ctx) {
        super.exitFunction(ctx);
    }

    @Override
    public void enterFunction_call(NoKeyboardParser.Function_callContext ctx) {
        super.enterFunction_call(ctx);
    }

    @Override
    public void exitFunction_call(NoKeyboardParser.Function_callContext ctx) {
        super.exitFunction_call(ctx);
    }

    @Override
    public void enterEquals(NoKeyboardParser.EqualsContext ctx) {
        super.enterEquals(ctx);
    }

    @Override
    public void exitEquals(NoKeyboardParser.EqualsContext ctx) {
        super.exitEquals(ctx);
    }

    @Override
    public void enterNot_equals(NoKeyboardParser.Not_equalsContext ctx) {
        super.enterNot_equals(ctx);
    }

    @Override
    public void exitNot_equals(NoKeyboardParser.Not_equalsContext ctx) {
        super.exitNot_equals(ctx);
    }

    @Override
    public void enterGreater(NoKeyboardParser.GreaterContext ctx) {
        super.enterGreater(ctx);
    }

    @Override
    public void exitGreater(NoKeyboardParser.GreaterContext ctx) {
        super.exitGreater(ctx);
    }

    @Override
    public void enterLess(NoKeyboardParser.LessContext ctx) {
        super.enterLess(ctx);
    }

    @Override
    public void exitLess(NoKeyboardParser.LessContext ctx) {
        super.exitLess(ctx);
    }

    @Override
    public void enterAdd(NoKeyboardParser.AddContext ctx) {
        super.enterAdd(ctx);
    }

    @Override
    public void exitAdd(NoKeyboardParser.AddContext ctx) {
        super.exitAdd(ctx);
    }

    @Override
    public void enterSub(NoKeyboardParser.SubContext ctx) {
        super.enterSub(ctx);
    }

    @Override
    public void exitSub(NoKeyboardParser.SubContext ctx) {
        super.exitSub(ctx);
    }

    @Override
    public void enterMul(NoKeyboardParser.MulContext ctx) {
        super.enterMul(ctx);
    }

    @Override
    public void exitMul(NoKeyboardParser.MulContext ctx) {
        super.exitMul(ctx);
    }

    @Override
    public void enterDiv(NoKeyboardParser.DivContext ctx) {
        super.enterDiv(ctx);
    }

    @Override
    public void exitDiv(NoKeyboardParser.DivContext ctx) {
        super.exitDiv(ctx);
    }

    @Override
    public void enterMod(NoKeyboardParser.ModContext ctx) {
        super.enterMod(ctx);
    }

    @Override
    public void exitMod(NoKeyboardParser.ModContext ctx) {
        super.exitMod(ctx);
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        super.visitErrorNode(node);
    }
}
