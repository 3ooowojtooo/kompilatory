package com.quary.wojciech.custom;

import com.quary.wojciech.generated.NoKeyboardBaseVisitor;
import com.quary.wojciech.generated.NoKeyboardParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class MyNoKeyboardVisitor extends NoKeyboardBaseVisitor<Void> {

    private Map<String, Object> variables;
    private Map<String, List<NoKeyboardParser.StatementContext>> functions;
    private Stack<Boolean> booleanStack;

    public MyNoKeyboardVisitor() {
        variables = new HashMap<>();
        booleanStack = new Stack<>();
        functions = new HashMap<>();
    }

    @Override
    public Void visitLoop(NoKeyboardParser.LoopContext ctx) {
        for(int a = getNumberFromExp(ctx.exp(0)) ; a < getNumberFromExp(ctx.exp(1)); a++){
            ctx.statement().forEach(this::visit);
        }
        return super.visitLoop(ctx);
    }

    @Override
    public Void visitPrint(NoKeyboardParser.PrintContext ctx) {
        String output;
        if (ctx.ID() != null) {
            output = variables.get(ctx.ID().getText()).toString();
        } else if (ctx.NUMBER() != null) {
            output = ctx.NUMBER().getText();
        } else {
            output = ctx.STRING().getText();
            output = output.substring(1, output.length() - 1);
        }
        System.out.println(output);
        return super.visitPrint(ctx);
    }



    @Override
    public Void visitAssign(NoKeyboardParser.AssignContext ctx) {
        // This method is called when the parser has finished
        // parsing the assign statement

        // Get variable name
        String variableName = ctx.ID(0).getText();

        // Get value from variable or number
        if(ctx.ID().size() > 1)
            variables.put(variableName, variables.get(ctx.ID(1).getText()));
        else if(ctx.NUMBER() != null){
            variables.put(variableName,Integer.parseInt(ctx.NUMBER().getText()));
        }else{
            String text = ctx.STRING().getText();
            variables.put(variableName,text.substring(1,text.length()-1));
        }
        return super.visitAssign(ctx);
    }

    @Override
    public Void visitAdd(NoKeyboardParser.AddContext ctx) {
        // This method is called when the parser has finished
        // parsing the add statement
        if(ctx.exp().size() > 1){
            String variableName = ctx.exp(1).ID().getText();
            int value = getNumberFromExp(ctx.exp(0));
            variables.put(variableName, ((Integer) variables.get(variableName)) + value);
        }

        return super.visitAdd(ctx);
    }

    @Override
    public Void visitSub(NoKeyboardParser.SubContext ctx) {
        // This method is called when the parser has finished
        // parsing the add statement
        if(ctx.exp().size() > 1){
            String variableName = ctx.exp(1).ID().getText();
            int value = getNumberFromExp(ctx.exp(0));
            variables.put(variableName, (Integer) variables.get(variableName) - value);
        }

        return super.visitSub(ctx);
    }
    private Integer getNumberFromExp(NoKeyboardParser.ExpContext expContext){
        return expContext.ID() != null ? (Integer) variables.get(expContext.ID().getText()) : Integer.parseInt(expContext.NUMBER().getText());
    }



    @Override
    public Void visitIf_block(NoKeyboardParser.If_blockContext ctx) {
        this.visit(ctx.children.get(1));
        if(booleanStack.pop()){
            ctx.statement().forEach(this::visit);
        }
        booleanStack.clear();
        return null;
    }

    @Override
    public Void visitEquals(NoKeyboardParser.EqualsContext ctx) {
        booleanStack.push(getNumberFromExp(ctx.exp(0)).equals(getNumberFromExp(ctx.exp(1))));
        return super.visitEquals(ctx);
    }

    @Override
    public Void visitNot_equals(NoKeyboardParser.Not_equalsContext ctx) {
        booleanStack.push(! getNumberFromExp(ctx.exp(0)).equals(getNumberFromExp(ctx.exp(1))));
        return super.visitNot_equals(ctx);
    }

    @Override
    public Void visitGreater(NoKeyboardParser.GreaterContext ctx) {
        booleanStack.push(getNumberFromExp(ctx.exp(0)) > (getNumberFromExp(ctx.exp(1))));
        return super.visitGreater(ctx);
    }

    @Override
    public Void visitLess(NoKeyboardParser.LessContext ctx) {
        booleanStack.push(getNumberFromExp(ctx.exp(0)) < (getNumberFromExp(ctx.exp(1))));
        return super.visitLess(ctx);
    }

    @Override
    public Void visitMul(NoKeyboardParser.MulContext ctx) {
        if(ctx.exp().size() > 1){
            String variableName = ctx.exp(1).ID().getText();
            int value = getNumberFromExp(ctx.exp(0));
            variables.put(variableName, (Integer) variables.get(variableName) * value);
        }
        return super.visitMul(ctx);
    }

    @Override
    public Void visitDiv(NoKeyboardParser.DivContext ctx) {
        if(ctx.exp().size() > 1){
            String variableName = ctx.exp(1).ID().getText();
            int value = getNumberFromExp(ctx.exp(0));
            variables.put(variableName, (Integer) variables.get(variableName) / value);
        }
        return super.visitDiv(ctx);
    }

    @Override
    public Void visitMod(NoKeyboardParser.ModContext ctx) {
        if(ctx.exp().size() > 1){
            String variableName = ctx.exp(1).ID().getText();
            int value = getNumberFromExp(ctx.exp(0));
            variables.put(variableName, (Integer) variables.get(variableName) % value);
        }
        return super.visitMod(ctx);
    }

    @Override
    public Void visitFunction(NoKeyboardParser.FunctionContext ctx) {
        functions.put(ctx.ID().getText(),ctx.statement());
        return null;
    }

    @Override
    public Void visitFunction_call(NoKeyboardParser.Function_callContext ctx) {
        functions.get(ctx.ID().getText()).forEach(this::visit);
        return super.visitFunction_call(ctx);
    }
}
