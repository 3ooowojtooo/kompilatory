package com.quary.wojciech;

import com.quary.wojciech.custom.MyNoKeyboardListener;
import com.quary.wojciech.custom.MyNoKeyboardVisitor;
import com.quary.wojciech.generated.NoKeyboardLexer;
import com.quary.wojciech.generated.NoKeyboardParser;
import com.quary.wojciech.generated.NoKeyboardVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class App
{
    public static void main( String[] args )
    {
        try {

            Scanner scanner = new Scanner(System.in);
            ANTLRInputStream input = new ANTLRInputStream(
                    new FileInputStream(scanner.nextLine()));

            NoKeyboardLexer lexer = new NoKeyboardLexer(input);
            NoKeyboardParser parser = new NoKeyboardParser(new CommonTokenStream(lexer));
            NoKeyboardVisitor visitor = new MyNoKeyboardVisitor();
            parser.addParseListener(new MyNoKeyboardListener());
            // Start parsing
            visitor.visit(parser.program());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
