// Generated from NoKeyboard.g4 by ANTLR 4.7.2
package com.quary.wojciech.generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link NoKeyboardParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface NoKeyboardVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(NoKeyboardParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(NoKeyboardParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#assign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(NoKeyboardParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(NoKeyboardParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(NoKeyboardParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(NoKeyboardParser.ExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#if_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_block(NoKeyboardParser.If_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(NoKeyboardParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#function_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call(NoKeyboardParser.Function_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#equals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquals(NoKeyboardParser.EqualsContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#not_equals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_equals(NoKeyboardParser.Not_equalsContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#greater}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreater(NoKeyboardParser.GreaterContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#less}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLess(NoKeyboardParser.LessContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(NoKeyboardParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#sub}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSub(NoKeyboardParser.SubContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#mul}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMul(NoKeyboardParser.MulContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#div}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiv(NoKeyboardParser.DivContext ctx);
	/**
	 * Visit a parse tree produced by {@link NoKeyboardParser#mod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMod(NoKeyboardParser.ModContext ctx);
}