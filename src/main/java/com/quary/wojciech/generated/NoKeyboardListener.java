// Generated from NoKeyboard.g4 by ANTLR 4.7.2
package com.quary.wojciech.generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link NoKeyboardParser}.
 */
public interface NoKeyboardListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(NoKeyboardParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(NoKeyboardParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(NoKeyboardParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(NoKeyboardParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(NoKeyboardParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(NoKeyboardParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(NoKeyboardParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(NoKeyboardParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#loop}.
	 * @param ctx the parse tree
	 */
	void enterLoop(NoKeyboardParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#loop}.
	 * @param ctx the parse tree
	 */
	void exitLoop(NoKeyboardParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExp(NoKeyboardParser.ExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExp(NoKeyboardParser.ExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#if_block}.
	 * @param ctx the parse tree
	 */
	void enterIf_block(NoKeyboardParser.If_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#if_block}.
	 * @param ctx the parse tree
	 */
	void exitIf_block(NoKeyboardParser.If_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(NoKeyboardParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(NoKeyboardParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(NoKeyboardParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(NoKeyboardParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#equals}.
	 * @param ctx the parse tree
	 */
	void enterEquals(NoKeyboardParser.EqualsContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#equals}.
	 * @param ctx the parse tree
	 */
	void exitEquals(NoKeyboardParser.EqualsContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#not_equals}.
	 * @param ctx the parse tree
	 */
	void enterNot_equals(NoKeyboardParser.Not_equalsContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#not_equals}.
	 * @param ctx the parse tree
	 */
	void exitNot_equals(NoKeyboardParser.Not_equalsContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#greater}.
	 * @param ctx the parse tree
	 */
	void enterGreater(NoKeyboardParser.GreaterContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#greater}.
	 * @param ctx the parse tree
	 */
	void exitGreater(NoKeyboardParser.GreaterContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#less}.
	 * @param ctx the parse tree
	 */
	void enterLess(NoKeyboardParser.LessContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#less}.
	 * @param ctx the parse tree
	 */
	void exitLess(NoKeyboardParser.LessContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#add}.
	 * @param ctx the parse tree
	 */
	void enterAdd(NoKeyboardParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#add}.
	 * @param ctx the parse tree
	 */
	void exitAdd(NoKeyboardParser.AddContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#sub}.
	 * @param ctx the parse tree
	 */
	void enterSub(NoKeyboardParser.SubContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#sub}.
	 * @param ctx the parse tree
	 */
	void exitSub(NoKeyboardParser.SubContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#mul}.
	 * @param ctx the parse tree
	 */
	void enterMul(NoKeyboardParser.MulContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#mul}.
	 * @param ctx the parse tree
	 */
	void exitMul(NoKeyboardParser.MulContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#div}.
	 * @param ctx the parse tree
	 */
	void enterDiv(NoKeyboardParser.DivContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#div}.
	 * @param ctx the parse tree
	 */
	void exitDiv(NoKeyboardParser.DivContext ctx);
	/**
	 * Enter a parse tree produced by {@link NoKeyboardParser#mod}.
	 * @param ctx the parse tree
	 */
	void enterMod(NoKeyboardParser.ModContext ctx);
	/**
	 * Exit a parse tree produced by {@link NoKeyboardParser#mod}.
	 * @param ctx the parse tree
	 */
	void exitMod(NoKeyboardParser.ModContext ctx);
}