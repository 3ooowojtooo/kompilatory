grammar NoKeyboard;
/*
* LEXER RULES
*/

ID     : [a-z]+ ;
NUMBER : [0-9]+ ;
STRING : '"' ('\'"' | ~ '"')* '"';
WS     : [ \n\t]+ -> skip;


/*
* PARSER RULES
*/
program   : (('functions' (function)+ 'begin')|'begin') (statement)+ 'end';

statement : assign | add | print | loop | exp | if_block | function_call;
assign    : 'let' ID 'be' (NUMBER | ID | STRING) ;
print     : 'print' (NUMBER | ID | STRING) ;
loop : 'loop' exp 'to' exp 'begin' statement+ 'end';

exp:
     add
   | sub
   | mul
   | div
   | mod
   | ID
   | NUMBER
   | STRING
;

if_block
    : 'if' (equals|not_equals|greater|less) 'begin' statement+ 'end';

function
    : 'function ' ID 'begin' statement+ 'end';

function_call
    : 'call' ID;

equals
   : 'is' exp 'same_as' exp
   ;

not_equals
   : 'is_not' exp 'same_as' exp
   ;

greater
   : 'is_greater' exp 'than' exp
   ;

less
   : 'is_smaller' exp 'than' exp
   ;

add
   : 'add' exp 'to' exp
   ;

sub
   : 'sub' exp 'from' exp
   ;

mul
   : 'multiply' exp 'and' exp
   ;

div
   : 'divide' exp 'by' exp
   ;

mod
   : 'mod' exp 'and' exp
   ;

